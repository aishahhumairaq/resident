import { Component } from '@angular/core';
import { CacheService } from 'ionic-cache';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  public url = [
    {
      title: 'Home',
      url: '/home',
      click: true
    },
    {
      title: 'Profile',
      url: '/profile',
      click: true
    },
    {
      title: 'Announcement',
      url: '/announcement',
      click: true
    },
    {
      title: 'Home Actions',
      url: '',
      click: false,
      children: [
        {
          title: 'Book A Facility',
          url: '/facility',
          click: true,
        },
        {
          title: 'View Invoice',
          url: '/invoice',
          click: true,
        },
        {
          title: 'Manage Visitor',
          url: '/manage-visitor',
          click: true,
        },
        {
          title: 'Complaints and Feedback',
          url: '/complaint-feedback',
          click: true,
        },
      ]
    },
    {
      title: 'Settings',
      click: true,
      url: '/settings'
    },
  ];

}
