import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../models/user.model';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/Operators'; 



@Injectable({ providedIn: 'root' })
export class AuthService {

    user: User;
    user$: Observable<User>;

    constructor(
        private afs: AngularFirestore,
        private afAuth: AngularFireAuth,
        private router: Router, 
        private loadingController: LoadingController,
        private toastController: ToastController,

        
    ) {
        
     } //end of constructor
}