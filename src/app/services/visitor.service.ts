import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Unit, Visitor } from '../models/unit.model';
import { LoadingService } from './loading.service';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class VisitorService {

  visitorCollection:AngularFirestoreCollection<Visitor>;
  visitor:Observable<Visitor[]>;
  id:any;
  unit:Unit;

  constructor(
    private afs: AngularFirestore,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private alertCtrl: AlertController,
    private modal: ModalController
  ) { }

  async getVisitorUsingUnitID(unitID) {
    console.log('unit', unitID)
    try {
      return this.visitor = await this.afs.collection('visitor', ref => ref.where("unitID", "==", unitID))
        .snapshotChanges().pipe(
          //take(1),
          map(actions => actions.map(a => {
            const data = a.payload.doc.data() as Visitor;
            data.visitorID = a.payload.doc.id;
            return { ...data };

          }))
        )
    }
    catch (e) {
      console.log('err', e);
    }
  }

  async registerVisitor(visitor, id, block, unitNum) {
    console.log('registervisitor', visitor, id);
    
    if (this.formValidation(visitor)) {
      //show loader
      this.loadingService.show()
      try {
        visitor.unitID = id;
        visitor.block = block;
        visitor.unitNum = unitNum;
        visitor.status = "Pending";
        await this.afs.collection("visitor").add(visitor)
        this.modal.dismiss()
        this.toastService.presentToast("Successfully register a visitor!")
      }
      catch (e) {
        this.toastService.presentToast(e);
      }
      this.loadingService.hide();
    }
  }

  formValidation(visitor) {
    var patternChar = new RegExp("^[a-z A-Z]+$");
    var patternVehicle = new RegExp("^[a-z A-Z 0-9]+$");
    var patternPhone = new RegExp("^[0-9]+$");

    if (!visitor.visName) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Please fill in the visitor name.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!visitor.visPhone) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Please fill in the visitor phone number.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!visitor.vehicle) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Please fill in the vehicle details.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!visitor.vehiclePlate) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Please fill in the vehicle details.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!patternChar.test(visitor.visName)) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Name should only have alphabets.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!patternVehicle.test(visitor.vehicle)) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Vehicle model should not have symbols.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!patternPhone.test(visitor.visPhone)) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Phone number should only have numbers.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!patternVehicle.test(visitor.vehiclePlate)) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Vehicle plate should not have symbols.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!visitor.visitDate) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Please fill in the visit date.',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else {
      return true;
    }
  }

}
