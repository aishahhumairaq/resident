import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { LoadingService } from './loading.service';
import { ToastService } from './toast.service';


@Injectable({ providedIn: 'root' })

export class registerResidentService {

    constructor(
        private toastService: ToastService,
        private loadingService: LoadingService,
        private afAuth: AngularFireAuth,
        private navCtrl: NavController,
        private firestore: AngularFirestore
    ) { }

    async registerResident(user, profile, unit) {
        if (this.formValidation(user)) {
            //show loader 
            this.loadingService.show();

            try {
                await this.afAuth.createUserWithEmailAndPassword(user.email, user.password).then(async data => {
                    console.log(data);
                    profile.userID = data.user.uid;
                    unit.userID = data.user.uid;
                    user.userID = data.user.uid;
                    profile.unitID = unit.unitID;
                    user.unitID = unit.unitID;

                    //create User table
                    await this.firestore.collection("users").doc(data.user.uid).set(user);
                    //create Profile table
                    await this.firestore.collection("profiles").add(profile).then(async prof => {
                        profile.profileID = prof.id;
                        await this.firestore.doc("profiles/" + profile.profileID).update({
                            profileID: profile.profileID,
                            userID: user.userID
                        })
                    });
                    //create Unit table
                    await this.firestore.collection("unit").add(unit);

                    //redirect to home page 
                    this.navCtrl.navigateRoot("home");
                })
            }
            catch (e) {
                this.toastService.presentToast(e);
                console.log(e);
            }
            //dismiss loader 
            this.loadingService.hide();
        }
    }

    formValidation(user) {
        if (!user.email) {
            this.toastService.presentToast("Enter email");
            return false;
        }
        if (!user.password) {
            this.toastService.presentToast("Enter password");
            return false;
        }
        else {
            return true;
        }

 
    }



}