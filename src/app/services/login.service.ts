import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User, Profile } from '../models/user.model';
import { LoadingService } from './loading.service';
import { ToastService } from './toast.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController } from '@ionic/angular';
import { Unit } from '../models/unit.model';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';


@Injectable({ providedIn: 'root' })

export class LoginService {

    unit: Unit = {
        status: '',
        userID: '',
        unitID: '',
        block: '',
        unitNum: '',
        parkingID: '',
    }

    user = {} as User;

    userDoc: AngularFirestoreDocument<User>;

    constructor(
        private toastService: ToastService,
        private loadingService: LoadingService,
        private afAuth: AngularFireAuth,
        private navCtrl: NavController,
        private afs: AngularFirestore

    ) { }

    async loginValidation(user) {
        if (this.formValidation(user)) {

            try {
                return await this.afAuth.signInWithEmailAndPassword(user.email, user.password).then(data => {
                    this.loadingService.show();
                    console.log('uid', data.user.uid);
                    return this.getUserID(data.user.uid)
                    // return this.getUnitID(data.user.uid).then(data => {
                    //     console.log('login info status', data);

                    //     if (data) {
                    //         //dismiss loader 
                    //         this.loadingService.hide();
                    //         return this.unit;
                    //     }
                    // })
                })
            }
            catch (e) {
                //display error
                this.toastService.presentToast(e);
                console.log(e);
                this.loadingService.hide();

            }
            //dismiss loader 
            this.loadingService.hide();
        }
    }

    formValidation(user) {
        if (!user.email) {
            this.toastService.presentToast("Enter email");
            return false;
        }
        if (!user.password) {
            this.toastService.presentToast("Enter password");
            return false;
        }
        return true;
    }

    async getUserID(id: string) {

        try {
            return await this.afs.collection("users").ref.where("__name__", "==", id)
                .get()
                .then((querySnapshot) => {

                    if (querySnapshot.empty) {
                        console.log('query is empty');
                        this.toastService.presentToast("It looks like this user does not exist!")
                        this.loadingService.hide();
                        return false;

                    }
                    else if (querySnapshot.size > 1) {
                        console.log('query returns more than 1 data')
                        return false;
                    }

                    else {
                        querySnapshot.forEach((doc) => {
                            //retrieve data in doc
                            let data: any = doc.data();
                            return this.user = {
                                userID: doc.id,
                                email: data.email,
                                password: data.password,
                                userType: data.userType,
                                firstLogin: data.firstLogin,
                                unitID: data.unitID,
                                profileID: data.profileID
                            }

                            // console.log('user have unit', this.unit);
                        });
                    }
                    this.loadingService.hide();
                    return this.user;

                })
        } catch (e) {
            console.log("Error getting user", e);

        }


    }

    //get Unit table based on UserID
    async getUnitID(id: string) {
        try {
            return await this.afs.collection("unit").ref.where("userID", "==", id)
                .get()
                .then((querySnapshot) => {

                    if (querySnapshot.empty) {
                        console.log('query is empty');
                        return false;

                    }
                    else if (querySnapshot.size > 1) {
                        console.log('query returns more than 1 data')
                        return false;
                    }

                    else {
                        querySnapshot.forEach((doc) => {
                            //retrieve data in doc
                            let data: any = doc.data();
                            this.unit.unitID = doc.id;
                            this.unit.userID = data.userID;
                            this.unit.block = data.block;
                            this.unit.unitNum = data.unitNum;
                            this.unit.parkingID = data.parkingID;
                            this.unit.status = data.status;

                        });
                    }

                    return this.unit;

                })
        } catch (e) {
            console.log("Error getting unit", e);
        }
    }


    updatePassword(id: string, _password: string) {
        this.userDoc = this.afs.doc(`users/${id}`);
        this.userDoc.update({ password: _password });
    }

}
