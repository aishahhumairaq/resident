import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Unit } from '../models/unit.model';
import { Profile, User } from '../models/user.model';
import { LoadingService } from './loading.service';
import { ToastService } from './toast.service';


@Injectable({ providedIn: 'root' })
export class MenuService {

    unit = {} as Unit;
    user = {} as User;
    profile = {} as Profile;

    constructor(
        private firestore: AngularFirestore,
    ) {

    }
    
    sendDetails(unit: Unit, user: User, profile: Profile) {
        this.unit = unit;
        this.user = user;
        this.profile = profile;
    }

    returnUnit() {
        return this.unit;
    }

    returnUser() {
        return this.user;
    }

    returnProfile() {
        console.log('get prof', this.profile);

        return this.profile;
    }

   
}