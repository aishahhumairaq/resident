import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { Announcement } from '../models/announcement.model';
import { Unit } from '../models/unit.model';
import { Profile } from '../models/user.model';
import { LoadingService } from './loading.service';
import { ToastService } from './toast.service';
import { map } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class HomeService {

    unit: Unit;
    category: any;
    announcement: any; 

    constructor(
        private firestore: AngularFirestore,
        private afs: AngularFirestore

    ) { }

    async getActions() {

        try {
            return new Observable<any> (observer => {
                console.log('observer')
                this.firestore.collection("home-resident", ref => ref.orderBy("num", "asc")).snapshotChanges().subscribe(async data => {
                  observer.next(data);
                })
            })
        }catch(e) {
            console.log("error", e);
        }
    }

    getActionCategory = (): any => {
        let actionTable = [
            {
                'action_id': 1, 
                'action_name': 'Book A Facility',
                'img': 'assets/imgs/facility.jpg',
                'url': 'facility',
            },
            {
                'action_id': 2,
                'action_name': 'Manage Visitor',
                'img': 'assets/imgs/visitor.jpg',
                'url': 'manage-visitor'
            },
            {
                'action_id': 3,
                'action_name': 'Complaints & Feedback',
                'img': 'assets/imgs/complaint.jpg',
                'url': 'complaint-feedback'
            },
            {
                'action_id': 4, 
                'action_name': 'View Invoice',
                'img': 'assets/imgs/invoice.jpg',
                'url': 'invoice'
            },
        ];

        return actionTable;
    }

    async getAnnouncement() {
        try {
            return this.announcement = await this.firestore.collection('announcement', ref => ref.orderBy('timestamp', 'desc'))
            .snapshotChanges().pipe(
                map(actions => actions.map(a => {
                    const data = a.payload.doc.data() as Announcement;
                    data.newsID = a.payload.doc.id;
                    return { ...data};
                    
                }))
            )
        }catch(e) {
            console.log("error announcement", e);
        }
        
        
    }

    async updateProfile(userID, user) {
        this.afs.doc(`users/${userID}`).update(user);
    }

}
