import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoadingService {

    loading:any;

    constructor(private loadingCtrl: LoadingController) { }

    async show() {
        this.loading = await this.loadingCtrl.create({
        message: 'Please wait...'
        });
        await this.loading.present();
      }
    
      hide() {
        this.loading.dismiss();
      }

    
}
