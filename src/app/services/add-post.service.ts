import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Announcement } from '../models/announcement.model';
import { LoadingService } from './loading.service';
import { ToastService } from './toast.service';

@Injectable({ providedIn: 'root' })
export class AddPostService {

    post: any;
    
    constructor(
        private toastService: ToastService,
        private loadingService: LoadingService,
        private firestore: AngularFirestore


    ) { }

    async entryValidation(post: Announcement) {

        let timestamp = new Date();
        if (this.formValidation(post)) {
            //show loader
            this.loadingService.show()

            try {
                await this.firestore.collection("announcement").add(post)

            }
            catch(e){
                this.toastService.presentToast(e);
            }
        this.loadingService.hide();

        }
    }
    formValidation(post) {
        if (!post.newsTitle) {
            this.toastService.presentToast("Enter announcement title");
            return false;
        }
        if (!post.newsText) {
            this.toastService.presentToast("Enter announcement details");
            return false;
        }
        return true;
    }

}