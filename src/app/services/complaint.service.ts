import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Unit } from '../models/unit.model';
import { Complaint } from '../models/complaint-feedback.model';
import { map } from 'rxjs/operators';
import { ToastService } from './toast.service';
import { LoadingService } from './loading.service';
import { AlertController, ModalController } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class ComplaintService {

    unit: Unit;
    complaintCollections: AngularFirestoreCollection<Complaint>;
    complaint: Observable<Complaint[]>;

    constructor(
        private firestore: AngularFirestore,
        private toastService: ToastService,
        private loadingService: LoadingService,
        private alertCtrl: AlertController,
        private modal: ModalController
    ) {

    }

    async getComplaint(id) {
        try {
            // return this.complaint = await this.firestore.collection('complaint').valueChanges();
            return this.complaint = await this.firestore.collection('complaint', ref => ref.where("unitID", '==', id))
                .snapshotChanges().pipe(
                    map(actions => actions.map(a => {
                        const data = a.payload.doc.data() as Complaint;

                        const id = a.payload.doc.id;
                        return { id, ...data };

                    }))
                )
            // return this.complaint = await this.firestore.collection('complaint', ref => ref.where("unitID", "==", id))
            // .valueChanges()

        } catch (e) {
            console.log("error complaint", e);
        }
    }

    async complaintValidation(complaint, id, block, unitNum) {
        let timestamp = new Date();
        console.log('comp details', id, unitNum, block );
        
        if (this.formValidation(complaint)) {
            //show loader
            this.loadingService.show()
            try {
                complaint.unitID = id;
                complaint.block = block;
                complaint.unitNum = unitNum;
                await this.firestore.collection("complaint").add(complaint)
                this.modal.dismiss()
                this.toastService.presentToast("Successfully submitted a complaint!")
            }
            catch (e) {
                this.toastService.presentToast(e);
                console.log('err', e);

            }
            this.loadingService.hide();
        }
    }

    formValidation(complaint) {
        
        if ((!complaint.complaintDetails) && (!complaint.complaintTitle)) {
            this.alertCtrl.create({
                header: 'Alert!',
                message: 'Please fill in the details.',
                buttons: [
                    'OK'
                ]
            }).then(res => {
                res.present()
            })
            return false;
        }
        else if (!complaint.complaintTitle) {
            this.alertCtrl.create({
                header: 'Alert!',
                message: 'Please fill in the complaint title.',
                buttons: [
                    'OK'
                ]
            }).then(res => {
                res.present()
            })
            return false;
        }
        else if (!complaint.complaintDetails) {
            this.alertCtrl.create({
                header: 'Alert!',
                message: 'Please fill in the complaint details.',
                buttons: [
                    'OK'
                ]
            }).then(res => {
                res.present()
            })
            return false;
        }
        
        else {
            return true;
        }
    }

}