import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Profile } from '../models/user.model';
import { map } from 'rxjs/operators';
import { Parking } from '../models/parking.model';

@Injectable({ providedIn: 'root' })
export class ProfileService {

    profile: Profile;
    profileCollection: AngularFirestoreCollection<Profile>;

    parking: Observable<Parking[]>
    parkingCollection: AngularFirestoreCollection<Parking[]>;

    constructor(
        private afs: AngularFirestore
    ) { 
        this.profileCollection = this.afs.collection<Profile>('profiles');
    }

    async getProfile(userID) {
        try {
            return await this.profileCollection.ref.where("userID","==", userID)
            .get()
            .then(querySnapshot => {
                if (querySnapshot.empty) {
                    console.log('query is empty');
                    

                }
                else if (querySnapshot.size > 1) {
                    console.log('query returns more than 1 data')
                    
                }

                else {
                    querySnapshot.forEach(doc => {
                        this.profile = doc.data() as Profile;
                        this.profile.profileID = doc.id;
                        console.log('here is ur profile', this.profile);
                        
                    });
                }
                return this.profile;

            })
        }catch(e) {
            console.log(e);
            
        }
    }

    // async getProfileUsingUserID(userID) {
    //     console.log('userID get', userID)
    //     try {
    //         return this.profile = await this.afs.collection('profiles', ref => ref.where("userID","==", userID))
    //         .snapshotChanges().pipe(
    //             map(actions => actions.map(a => {
    //                 console.log('profile hello');
                    
    //                 const data = a.payload.doc.data() as Profile;
    //                 data.profileID = a.payload.doc.id;
    //                 return { ...data};
                    
    //             }))
    //         )
    //     }
    //     catch(e) {
    //         console.log('err', e); 
    //     }
    // }

    async getParkingUsingUnitID(unitID) {
        try {
            return this.parking = await this.afs.collection('parking', ref => ref.where("unitID","==", unitID))
            .snapshotChanges().pipe(
                map(actions => actions.map(a => {
                    const data = a.payload.doc.data() as Parking;
                    data.parkingID = a.payload.doc.id;
                    return { ...data};
                }))
            )
        }
        catch(e) {
            console.log('err', e); 
        }
    }

    
}
