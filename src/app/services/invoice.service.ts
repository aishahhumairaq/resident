import { Inject, Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Invoice } from '../models/invoice.model';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })

export class InvoiceService {

    // invoice: Invoice = {
    //     invoiceID: '',
    //     unitID: '',
    //     block: '', 
    //     unitNum: '',
    //     category: '',
    //     amountInvoice: '',
    //     dueDate: '',
    //     document: '',
    // }
    invoice: Observable<Invoice[]>;

     
    constructor(
        private afs: AngularFirestore,

     ) {

    }

    async getInvoiceUsingUnitID(id) {
        try {
            return this.invoice = await this.afs.collection('invoice ', ref => ref.where("unitID","==", id))
            .snapshotChanges().pipe(
                map(actions => actions.map(a => {
                    const data = a.payload.doc.data() as Invoice;
                    data.invoiceID = a.payload.doc.id;
                    return { ...data};
                    
                }))
            )
            // return await new Observable<any>(observer => {
            //     this.firestore.collection("invoice ", ref => ref.where("unitID", "==", id))
            //     .snapshotChanges().subscribe(data => {
            //         observer.next(data);
            //         console.log('invoice', data);
            //     })

            // })
        } catch (e) {
            console.log('e', e);
        }

    
        // return await this.afs.collection("invoice ").ref.where("unitID", "==", id)
        // .get()
        // .then((querySnapshot) => {
        //     if (querySnapshot.empty) {
        //         console.log('query is empty');
        //     //return false
                
        //     }
        //     else {
        //        querySnapshot.forEach(doc => {
        //             //retrieve data in doc
        //             let data: any = doc.data();
        //             this.invoice.invoiceID = doc.id; 
        //             this.invoice.unitID = data.unitID;
        //             this.invoice.block = data.block;
        //             this.invoice.unitNum = data.unitNum;
        //             this.invoice.category = data.category;
        //             this.invoice.amountInvoice = data.amountInvoice;
        //             this.invoice.dueDate = data.dueDate; 
        //             this.invoice.document = data.document;

        //             console.log('unit have invoice', this.invoice);
                    
        //         });
                
        //         return this.invoice; 
        //     }
        // });

        // .catch((error) => {
        //     console.log("Error getting documents: ", error);
        
        // });
    }

    //     try {
    //         return new Observable<any>(observer => {
    //             console.log('invoice service ts')
    //             this.firestore.collection("invoice ").snapshotChanges().subscribe(async data => {

    //                 observer.next(data);
    //                 console.log('invoice', data);
    //                 return this.getInvoiceUsingUnitID(data).then(data => {
    //                     console.log('invoice based on unitID', data)

    //                 }) 
                    
                       
    //             })
    //         })

    //     }catch(e) {
    //         console.log("error", e);
    //     }
    // }

}

    // imageDetailList: AngularFireList<any>;
    // fileList: any[];
    // dataSet: Data = {
    //     id:'',
    //     url:''
    // };
    // msg:string = 'error';

    // constructor(@Inject (AngularFireDatabase) private firebase: AngularFireDatabase ) 
    // { 

    // }

    // getImageDetailList() {
    //     this.imageDetailList = this.firebase.list('imageDetails');
    // }

    // insertImageDetails(id,url) {
    //     this.dataSet = {
    //       id : id,
    //       url: url
    //     };
    //     this.imageDetailList.push(this.dataSet);
    // }

    // getImage(value){
    //     this.imageDetailList.snapshotChanges().subscribe(
    //       list => {
    //         this.fileList = list.map(item => { return item.payload.val();  });
    //         this.fileList.forEach(element => {
    //           if(element.id===value)
    //           this.msg = element.url;
    //         });
    //         if(this.msg==='error')
    //           alert('No record found');
    //         else{
    //           window.open(this.msg);
    //           this.msg = 'error';
    //         }
    //       }
    //     );
    //   }
    // }

    // export interface Data{
    //   id:string;
    //   url:string;
    // }

    