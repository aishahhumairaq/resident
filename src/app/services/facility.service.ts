import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Booking, Facility } from '../models/facility.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FacilityService {

  facility: Observable<Facility[]>
  slots: Observable<any>
  booking: Observable<any>

  constructor(
    private firestore: AngularFirestore
  ) { }

  
  async getFacility() {
    try {
      return this.facility = await this.firestore.collection('facility')
        .snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data() as Facility;
            data.facilityID = a.payload.doc.id;
            return { ...data }
          }))
        )
    }
    catch (e) {
      console.log('err', e);

    }
  }

  async getUnitBooking(unitID) {
    try {
      return this.booking = await this.firestore.collection("user-booking", ref => ref.where("unitID", "==", unitID))
      .snapshotChanges().pipe(
        map(actions => actions.map (a => {
          const data = a.payload.doc.data() as Booking;
          data.id = a.payload.doc.id;
          return {...data}
        }))
      )
    }
    catch(e) {
      console.log('err', e);
      
    }
  }

  async getSlots(facilityID) {
    try {
      return this.slots = await this.firestore.collection("facility").doc(facilityID).collection("operation-hour")
      .snapshotChanges().pipe(
        map(actions => actions.map (a => {
          const data = a.payload.doc.data();
          data.id = a.payload.doc.id;
          return {...data};
        }))
      )
      
    }
    catch(e) {
      console.log('err', e);
      
    }
  }

  //get booked slots from booking table in firebase
  async getBooking(facilityID, date) {
    try {
      return this.booking = await this.firestore.collection("booking").doc(facilityID).collection(date)
      .snapshotChanges().pipe(
        map(actions => actions.map (a => {
          const slot = a.payload.doc.data();
          return {...slot};
        }))
      )
    }
    catch(e) {
      console.log('err', e);
      
    }

    
  }

  // //get booked slots from booking table in firebase
  // async getBookedSlots(facilityID, date, unitID) {
  //   try {
  //     return this.booking = await this.firestore.collection("booking").doc(facilityID).collection(date, ref => ref.where("unitID", "==", unitID))
  //     .snapshotChanges().pipe(
  //       map(actions => actions.map (a => {
  //         const unavailableslots = a.payload.doc.data() as Unavailable;
  //         return {...unavailableslots};
  //       }))
  //     )
  //   }
  //   catch(e) {
  //     console.log('err', e);
      
  //   }
  // }

}
