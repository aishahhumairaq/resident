import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ToastService {

    constructor(public toastController: ToastController) { }

    async presentToast(message: string) {
        const toast = await this.toastController.create({
            'message': message,
            'duration': 3000,
            position: 'bottom',
            cssClass: 'custom-toast'
          });
          toast.present();
    }

    
}
