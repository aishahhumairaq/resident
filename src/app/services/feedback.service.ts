import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Unit } from '../models/unit.model';
import { Feedback } from '../models/complaint-feedback.model';
import { map } from 'rxjs/operators';
import { ToastService } from './toast.service';
import { LoadingService } from './loading.service';
import { AlertController, ModalController } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class FeedbackService {

    unit: Unit;
    feedbackCollection: AngularFirestoreCollection<Feedback>;
    feedback: Observable<Feedback[]>;

    constructor(
        private firestore: AngularFirestore,
        private toastService: ToastService,
        private loadingService: LoadingService,
        private modal: ModalController,
        private alertCtrl: AlertController
    ) {

    }

    async getFeedback(id) {

        try {
            return this.feedback = await this.firestore.collection("feedback", ref => ref.where("unitID", "==", id))
            .valueChanges()
            // return this.feedback = await this.firestore.collection('feedback').snapshotChanges().pipe(
            //     map(actions => actions.map(a => {
            //         const data = a.payload.doc.data() as Feedback;
            //         const id = a.payload.doc.id;
            //         return { id, ...data };
            //     }))
            // )
        }
        catch(e) {
            console.log('feedback err', e)
        }
        // try {
        //     return new Observable<any>(observer => {
        //         this.firestore.collection("feedback", ref => ref.where("unitID", "==", id))
        //         .snapshotChanges().subscribe(data => {
        //             observer.next(data);
        //             console.log('feedback', data);
        //         })
        //     })

        // }catch(e) {
        //     console.log('error feedback',e)
        // }

    }

    async feedbackValidation(feedback, id, unitNum, block) {
        let timestamp = new Date();

        if (this.formValidation(feedback)) {
            //show loader
            this.loadingService.show()
            try {
                feedback.unitID = id;
                feedback.unitNum = unitNum;
                feedback.block = block;
                await this.firestore.collection("feedback").add(feedback)
                this.modal.dismiss()
                this.toastService.presentToast("Successfully submitted a feedback!")
            }
            catch(e){
                this.toastService.presentToast(e);
                console.log('err', e);
                
            }
        this.loadingService.hide();
        }
    }

    formValidation(feedback) {
        if ((!feedback.feedbackCategory) && (!feedback.Details)) {
            this.alertCtrl.create({
                header: 'Alert!',
                message: 'Please fill in the details.',
                buttons: [
                    'OK'
                ]
            }).then(res => {
                res.present()
            })
            return false;
        }
        else if(!feedback.feedbackCategory) {
            this.alertCtrl.create({
                header: 'Alert!',
                message: 'Please fill in the details.',
                buttons: [
                    'OK'
                ]
            }).then(res => {
                res.present()
            })
            return false;
        }
        else if(!feedback.feedbackDetails) {
            this.alertCtrl.create({
                header: 'Alert!',
                message: 'Please fill in the details.',
                buttons: [
                    'OK'
                ]
            }).then(res => {
                res.present()
            })
            return false;
        }
             
        else {
            return true;
        }
        // if (!feedback.feedbackDetails) {
        //     this.toastService.presentToast("Enter feedback details");
        //     return false;
        // }
        
    }

}