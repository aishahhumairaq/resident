import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Announcement } from '../models/announcement.model';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class announcementService {

    announcement: Observable<Announcement[]>;

    constructor(
        private firestore: AngularFirestore
    ) {
    }

    async getAnnouncement() {
        try {
            return this.announcement = await this.firestore.collection('announcement', ref => ref.orderBy('timestamp', 'desc'))
            .snapshotChanges().pipe(
                map(actions => actions.map(a => {
                    const data = a.payload.doc.data() as Announcement;
                    
                    data.newsID = a.payload.doc.id;
                    return { ...data};
                    
                }))
            )
    
        } catch (e) {
            console.log("error announcement", e);
        }
    }

}