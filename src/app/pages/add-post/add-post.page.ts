import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Announcement } from 'src/app/models/announcement.model';
import { AddPostService } from 'src/app/services/add-post.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.page.html',
  styleUrls: ['./add-post.page.scss'],
})
export class AddPostPage implements OnInit {

  post: Announcement = {
    editText: '',
    newsID: '',
    newsTitle: '',
    newsText: '',
    newsImage: '',
    timestamp: new Date

  }

  selectedFile: any;
  
  constructor(
    private navCtrl: NavController,
    private firestore: AngularFirestore,
    private location: Location,
    private addPostService: AddPostService
  ) { }

  ngOnInit() {
  }

  async createPost(post: Announcement) {

    this.addPostService.entryValidation(post).then(res => {
      
      this.navCtrl.navigateRoot("announcement");
    })
    

  }

  chooseImage(event) {
    this.selectedFile = event.target.file
  }

  cancel() {
    this.location.back();
  }


}
