import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { shareReplay } from 'rxjs/operators';
import { Parking } from 'src/app/models/parking.model';
import { Unit } from 'src/app/models/unit.model';
import { Profile, User } from 'src/app/models/user.model';
import { MenuService } from 'src/app/services/menu.service';
import { ProfileService } from 'src/app/services/profile.service'
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user = {} as User;
  profile: Profile = {
    profileID: '',
    userID: '',
    parkingID: '',
    fullName: '',
    icNum: '',
    birthDate: '',
    gender: '', // 0: female, 1: male
    unitID: '',
  }

  unit = {} as Unit;
  parking = {} as Parking;

  buttonText = "EDIT";
  disabled: boolean = true;

  constructor(
    private router: Router,
    private profileService: ProfileService,
    private firestore: AngularFirestore,
    private toastService: ToastService,
    private menuService: MenuService,
    private alertCtrl: AlertController

  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.profile = this.router.getCurrentNavigation().extras.state.profile;
      this.user = this.router.getCurrentNavigation().extras.state.user;
      this.unit = this.router.getCurrentNavigation().extras.state.unit;

    }
    else {
      this.profile = this.menuService.returnProfile();
      this.unit = this.menuService.returnUnit()
      this.user = this.menuService.returnUser();
      console.log('else ');

    }
    this.getParking(this.unit.unitID)

  }

  ngOnInit() {

    this.getProfile();
  }

  async getProfile() {
    await this.profileService.getProfile(this.unit.userID).then(res => {
      this.profile = res
      console.log('prof', this.profile);
    });
  }

  async getParking(unitID: string) {
    (await this.profileService.getParkingUsingUnitID(unitID)).pipe(shareReplay()).subscribe(data => {
      this.parking = data[0];
      console.log('unit parking', this.parking)
    })
  }

  async profileValidation(profile) {
    var patternChar = new RegExp("^[a-z A-Z]+$");
    var patternIC = new RegExp("^[0-9]+$");

    try {
      if (profile.fullName == '' || profile.icNum == null || profile.birthDate == '' || profile.gender == null || profile.gender == '') {
        this.alertCtrl.create({
          header: 'Alert!',
          message: 'Please fill in the details.',
          buttons: [
            'OK'
          ]
        }).then(res => {
          res.present();
        })
      }
      else if (profile.fullName == '') {
        this.alertCtrl.create({
          header: 'Alert!',
          message: 'Please fill in your full name before submitting your information.',
          buttons: [
            'OK'
          ]
        }).then(res => {
          res.present();
        })
      }
      else if (profile.gender == '' && profile.gender == null) {
        this.alertCtrl.create({
          header: 'Alert!',
          message: 'Please fill in the details.',
          buttons: [
            'OK'
          ]
        }).then(res => {
          res.present();
        })
      }
      if (!patternChar.test(profile.fullName)) {
        this.alertCtrl.create({
          header: 'Alert!',
          message: 'Name can only have alphabets.',
          buttons: [
            'OK'
          ]
        }).then(res => {
          res.present()
        })

      }
      else if (!patternIC.test(profile.icNum)) {
        this.alertCtrl.create({
          header: 'Alert!',
          message: 'IC Num can only have numbers.',
          buttons: [
            'OK'
          ]
        }).then(res => {
          res.present()
        })

      }
      else {
        await this.firestore.doc('profiles/' + profile.profileID).update(profile);
        console.log('update', profile)
        this.toastService.presentToast('You have successfully updated your profile!')
      }
      this.buttonText = 'SAVE';
      this.disabled = false;


    }
    catch (e) {
      console.log('err', e);

    }
    // if (!patternChar.test(profile.fullName)) {
    //   this.alertCtrl.create({
    //     header: 'Alert!',
    //     message: 'Name can only have alphabets.',
    //     buttons: [
    //       'OK'
    //     ]
    //   }).then(res => {
    //     res.present()
    //   })

    // }
    // else if (profile.fullName != '' || profile.icNum != '' || profile.birthDate != '' && profile.gender != '') {
    //   this.alertCtrl.create({
    //     header: 'Alert!',
    //     message: 'Please fill in the text fields before submitting your information.',
    //     buttons: [
    //       'OK'
    //     ]
    //   }).then(res => {
    //     res.present();
    //   })
    // }
    // else if ((profile.fullName != '' && profile.fullName != Number) && profile.icNum != '' && profile.birthDate != '' && profile.gender != '') {
    //   await this.firestore.doc('profiles/' + profile.profileID).update(profile);
    //   console.log('update', profile)
    //   this.toastService.presentToast('You have successfully updated your profile!')
    // }
    // else {
    //   this.buttonText = 'SAVE';
    //   this.disabled = false;
    // }
  }

  editProfile(disabled, profile: Profile) {
    this.disabled = !disabled;

    if (this.disabled) {
      this.buttonText = 'EDIT'
      this.profileValidation(profile);
    }
    else if (!this.disabled) {
      this.buttonText = 'SAVE'
    }

  }


}
