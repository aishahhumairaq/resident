import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { Unit } from 'src/app/models/unit.model';
import { HomeService } from 'src/app/services/home.service';
import { Profile, User } from 'src/app/models/user.model';
import { PostComponent } from 'src/app/components/post/display-post/post.component';
import { shareReplay } from 'rxjs/operators';
import { ProfileService } from 'src/app/services/profile.service';
import { MenuService } from 'src/app/services/menu.service';
import { FirstProfileComponent } from 'src/app/components/first-profile/first-profile.component';
import { Announcement } from 'src/app/models/announcement.model';
import { Plugins } from '@capacitor/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoginService } from 'src/app/services/login.service';
const { SplashScreen } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  unit: Unit = {
    status: '',
    userID: '',
    unitID: '',
    block: '',
    unitNum: '', 
    parkingID: '',
  }

  user = {} as User;
  profile: Profile = {
    profileID: '',
    userID: '',
    parkingID: '',
    fullName: '',
    icNum: '',
    birthDate: '',
    gender: '',
    unitID: '',
  };
  announcement: Announcement[];
  actionCategory: any;
  category: any;

  constructor(
    private router: Router,
    private homeService: HomeService,
    private afAuth: AngularFireAuth,
    private modal: ModalController,
    private profileService: ProfileService,
    private menuService: MenuService,
    private alertCtrl: AlertController,
    private firestore: AngularFirestore,
    private loginService: LoginService,
  
  ) 
  { 
    if (this.router.getCurrentNavigation().extras.state) {
      this.unit = this.router.getCurrentNavigation().extras.state.unit;
      this.user = this.router.getCurrentNavigation().extras.state.user;

    }
    // else {
    //   this.unit = this.menuService.returnUnit()
    //   this.user = this.menuService.returnUser()
    //   this.profile = this.menuService.returnProfile();
    // }
    this.getAnnouncements();
    this.actionCategory = this.homeService.getActionCategory();
    
  }
  
  ionViewWillEnter() {
    this.auth();
    SplashScreen.hide();
  }
  
  ngOnInit() {
    console.log('user', this.user);
  }
  
  auth() {
    this.afAuth.onAuthStateChanged( user => {
      if(user){
        console.log("USER:", user);
        console.log('lololol', user.email, user.uid);
        
        const result = this.firestore.doc(`users/${user.uid}`);
        var userProfile = result.valueChanges();
        userProfile.subscribe( res => {
          console.log('resreres', res);
          console.log('popopopo', this.user);

          this.user.email = user.email;
          this.user.userID = user.uid;
          this.user.firstLogin = res['firstLogin'];
          this.user.password = res['password'];
          this.user.profileID = res['profileID'];
          this.user.unitID = res['unitID'];
          this.user.userType = res['userType']
          
          this.getUnit(this.user.userID);
          this.checkFirstLogin();
        })

      }
    })
  }

  async getUnit(userID) {
    this.loginService.getUnitID(userID).then(unit => {
      console.log('get units', unit)
      if(unit) {
        this.unit = unit;
      }
      this.getProfile(this.user.userID);
    })
  }

  async getProfile(userID) {
    this.profileService.getProfile(userID).then(profile => {
      console.log('get profile', profile);
      
      if(profile) {
        this.profile = profile;
      }
      this.menuService.sendDetails(this.unit, this.user, this.profile);
    })
  }

  async checkFirstLogin() {

    if(this.user.firstLogin == true) {
      let firstLogin = await this.alertCtrl.create({
        header: 'Welcome!',
        message: 'Before you can proceed, you will need to update your profile information first.',
        buttons: [
          {
            text: 'OKAY',
            handler: (profile) => {
              this.updateProfile(profile);
            }
  
          }
        ]
      })
  
      await firstLogin.present();
    }
    
  }

  async updateProfile(profile) {
    let user = {
      firstLogin: false
    };

    this.homeService.updateProfile(this.user.userID, user).then(() => {
      
    });
    console.log('modal first profile', this.user.userID);
    let updateProfile = await this.modal.create({
      component: FirstProfileComponent,
      componentProps: {
        'userID': this.user.userID

      }
    })
    await updateProfile.present();

  }

  // async getCategory() {
  //   (await this.homeService.getActions()).subscribe(data => {
  //     this.category = data.map(e => {
  //       return {
  //         id: e.payload.doc.id,
  //         img: e.payload.doc.data()["img"],
  //         url: e.payload.doc.data()["url"],
  //         name: e.payload.doc.data()["name"],
  //       }
  //     })
  //     console.log('actions', this.category);

      
  //   });
    
  // }
  
  doAction(i) {

    console.log(this.actionCategory[i].url);
    let navigationExtras: NavigationExtras = {
      state:  {
        'data': this.unit
      }
    }
    this.router.navigate([this.actionCategory[i].url], navigationExtras)
    .catch(err => {
      console.log("Page has not been created!", err)
    })
  }

  async goToProfile(userID) {
 
    await this.profileService.getProfile(userID).then(res => {
      if(res) {
        let navigationExtras: NavigationExtras = {
          state: {
            'profile': res,
            'user': this.user,
            'unit': this.unit 
          }
        }
        this.router.navigate(['/profile'], navigationExtras);
      
      }
    });
  }


  //display announcement kat home page
  async getAnnouncements() {
    
    (await this.homeService.getAnnouncement()).pipe(shareReplay()).subscribe(res => {
      this.announcement = res; 
      console.log('announcement', this.announcement)



      for (let i = 0; i < this.announcement.length; i++) {
        //limit char
        this.announcement[i].editText = this.announcement[i].newsText.slice(0,145) + '...'; 
      }
    })

  
  }


  //direct to specific post page
  async goToPostPage(post) {

    const postModal = await this.modal.create({
      component: PostComponent,
      cssClass: 'post-modal',
      componentProps: {
        'post': post,

      }
    });
    console.log('announcement post', post)
    return await postModal.present();

  }
  

  //direct to overall post page
  goToAnnouncementPage() {

    let navigationExtras: NavigationExtras = {
      state:  {
        'announcements': this.announcement,
      }
      
    }
    this.router.navigate(['/announcement'], navigationExtras)

    .catch(err => {
      console.log("Button is not working")
    })
  }

  goToSettings() {
    let navigationExtras: NavigationExtras = {
      state: {
        'user': this.user,
      }
    }
    this.router.navigate(['/settings'], navigationExtras)

    .catch(err => {
      console.log('Data is not passed to settings page');
      
    })
  }
}
