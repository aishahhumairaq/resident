import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavigationExtras, Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Unit } from 'src/app/models/unit.model';
import { Profile, User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { registerResidentService } from 'src/app/services/register.service';
const { SplashScreen } = Plugins;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  user: User = {
    userID: '',
    email: '',
    password: '',
    userType: null, // 0: management, 1: resident
    firstLogin: null, // default: true
    unitID: '',
    profileID: '',
  }

  profile: Profile = {
    profileID: '',
    userID: '',
    fullName: '',
    icNum: '',
    birthDate: '',
    parkingID: '',
    gender: '',
    unitID: ''
  }

  unit: Unit = {
    status: '',
    userID: '',
    unitID: '',
    block: '',
    unitNum: '',
    parkingID: '',
  }


  constructor(
    private loginService: LoginService,
    private router: Router,
    private alertCtrl: AlertController,
    private registerService: registerResidentService

  ) { }

  ionViewWillEnter() {
    SplashScreen.hide();
  }

  ngOnInit() {
  }

  register(user: User, profile: Profile, unit: Unit) {
    console.log("user", user); 
    console.log("profile", profile);
    console.log("unit", unit);

    this.registerService.registerResident(user, profile, unit);
  }

  async login(user: User) {

    this.loginService.loginValidation(user).then(user => {
      if (user) {
        if (user.userType == 1) { //is a resident
          this.loginService.getUnitID(user.userID).then(unit => {
            console.log('unit', unit);

            let navigationExtras: NavigationExtras = {
              state: {
                'unit': unit,
                'user': user,
              }
            }
            this.router.navigate(['/home'], navigationExtras);
          })
        }
        else if (user.userType == 0) {
          this.alertCtrl.create({
            header: 'Alert!',
            message: 'You are an admin, thus you have no access to the resident application.',
            buttons: [
              'OK'
            ]
          }).then(res => {
            res.present();
          });
        }
        else {
          // this.alertCtrl.create({
          //   header: 'Alert!',
          //   message: 'You have to be a registered resident in order for you to login.',
          //   buttons: [
          //     'OK'
          //   ]
          // }).then(res => {
          //   res.present();
          // });
        }
      }


    })
  }

}

