import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { PostComponent } from 'src/app/components/post/display-post/post.component';
import { Announcement } from 'src/app/models/announcement.model';
import { Unit } from 'src/app/models/unit.model';
import { announcementService } from 'src/app/services/announcement.service';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.page.html',
  styleUrls: ['./announcement.page.scss'],
})
export class AnnouncementPage implements OnInit {

  announcement: Announcement[];
  unit: Unit;

  constructor(
    private router: Router,
    private modal: ModalController,
    private announcementService: announcementService,
    
  ) { 
    if(this.router.getCurrentNavigation().extras.state) {
      this.announcement = this.router.getCurrentNavigation().extras.state.announcements;
      console.log('announcement page ts', this.announcement);
    }
    this.displayAnnouncement();
  }

  ngOnInit() {
  }

  async displayAnnouncement() {

    (await this.announcementService.getAnnouncement()).pipe(shareReplay()).subscribe(res => {
      this.announcement = res; 
      console.log('announcement', this.announcement)
    })

  }

  async goToPostPage(post) {

    const postModal = await this.modal.create({
      component: PostComponent,
      cssClass: 'post-modal',
      componentProps: {
        'post': post

      }
    });
    console.log('announcement post', post)
    return await postModal.present();

  }

}
