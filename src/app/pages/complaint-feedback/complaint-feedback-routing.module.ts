import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComplaintFeedbackPage } from './complaint-feedback.page';

const routes: Routes = [
  {
    path: '',
    component: ComplaintFeedbackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComplaintFeedbackPageRoutingModule {}
