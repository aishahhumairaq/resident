import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Complaint, Feedback } from 'src/app/models/complaint-feedback.model';
import { Unit } from 'src/app/models/unit.model';
import { ComplaintService } from 'src/app/services/complaint.service';
import { FeedbackService } from 'src/app/services/feedback.service';
import { shareReplay } from 'rxjs/operators';
import { MenuService } from 'src/app/services/menu.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-complaint-feedback',
  templateUrl: './complaint-feedback.page.html',
  styleUrls: ['./complaint-feedback.page.scss'],
})
export class ComplaintFeedbackPage implements OnInit {

  value = "all";

  complaint: Complaint[]; 
  feedback: Feedback[];

  unit: Unit = {
    status: '',
    userID: '',
    unitID: '',
    block: '',
    unitNum: '', 
    parkingID: '',
  }

  user = {} as User;

  constructor(
    private router: Router,
    private complaintService: ComplaintService,
    private feedbackService: FeedbackService,
    private menuService: MenuService
  ) 
  { 
    if(this.router.getCurrentNavigation().extras.state) {
      this.unit = this.router.getCurrentNavigation().extras.state.data;
      console.log('complaint feedback', this.unit);
    }
    // else {
    //   this.unit.unitID = "N4L5oI8ztTNDJPUcwahc"
    //   this.unit.unitNum = "01-01"
    //   this.unit.block = "A"
    // }

    if(!this.user.email) {
      this.unit = this.menuService.returnUnit();
      this.user = this.menuService.returnUser();
    }

    this.getFeedbacks(this.unit.unitID);
    this.getComplaints(this.unit.unitID);
  }

  ngOnInit() { 
  
  }

  segmentChanged(event){
    this.value = event.target.value
    console.log(this.value);

  }

  ionViewWillEnter() {
    
  }

  async getComplaints(unitID: string) {

    (await this.complaintService.getComplaint(unitID)).pipe(shareReplay()).subscribe( data => {
      this.complaint = data; 
      console.log('complaint ts', this.complaint)
    })


    // (await this.complaintService.getComplaint(unitID)).subscribe(data => {
    //   this.complaint = data.map(e => {
    //     return {
    //       complaintID: e.payload.doc.id,
    //       unitID: e.payload.doc.data()["unitID"],
    //       complaintTitle: e.payload.doc.data()["complaintTitle"],
    //       complaintDetails: e.payload.doc.data()["complaintDetails"],
    //       complaintDate: e.payload.doc.data()["complaintDate"]
    //     }
    //   })
    //   console.log('complaint page', this.complaint)

    // });
  }

  async getFeedbacks(unitID: string) {

    (await this.feedbackService.getFeedback(unitID)).pipe(shareReplay()).subscribe(res => {
      this.feedback = res; 
      console.log('feedback ts', this.feedback)
    })

  }

}
