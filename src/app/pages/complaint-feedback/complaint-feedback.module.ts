import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComplaintFeedbackPageRoutingModule } from './complaint-feedback-routing.module';

import { ComplaintFeedbackPage } from './complaint-feedback.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComplaintFeedbackPageRoutingModule,
    ComponentModule,

  ],
  declarations: [ComplaintFeedbackPage]
})
export class ComplaintFeedbackPageModule {}
