import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { MenuService } from 'src/app/services/menu.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  user = {} as User;
  currentUser: any;

  constructor(
    private menuService: MenuService,
    private router: Router,
    private alertCtrl: AlertController,
    private loginService: LoginService,
    private afAuth: AngularFireAuth,
    private toastService: ToastService

  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.user = this.router.getCurrentNavigation().extras.state.user;
      console.log('settings get user data', this.user);

    }
    else {
      this.user = this.menuService.returnUser();
      console.log('obtain from menu service', this.user);

    }
    this.currentUser = this.afAuth.currentUser;
  }

  ngOnInit() {

  }

  async changePasswordAlert() {
    // let passwordAlert = await this.alertCtrl.create({
    //   header: 'Change password',
    //   message: "Enter your current password and new password",
    //   inputs: [
    //     {
    //       name: 'currentP',
    //       type: 'password',
    //       placeholder: 'Current password'
    //     },
    //     {
    //       name: 'newP',
    //       type: 'password',
    //       placeholder: 'New password (6 characters or more)'
    //     },
    //     {
    //       name: 'newP2',
    //       type: 'password',
    //       placeholder: 'New password (6 characters or more)'
    //     },
    //   ],
    //   buttons: [
    //     {
    //       text: 'Cancel',
    //       role: 'cancel',
    //       cssClass: 'secondary',
    //       handler: (blah) => {
    //       }
    //     },
    //     {
    //       text: 'Confirm',
    //       handler: (pw) => {
    //         if (pw.currentP == this.user.password) {
    //           if (pw.newP != pw.newP2) { //passwords don't match
    //             this.alertCtrl.create({
    //               header: 'Alert!',
    //               message: 'New passwords do not match!',
    //               buttons: [
    //                 'OK'
    //               ]
    //             }).then(res => {
    //               res.present()
    //             })
    //           }
    //           else if (pw.newP.length < 6) { //password length less than 6
    //             this.alertCtrl.create({
    //               header: 'Alert!',
    //               message: 'New password too short!',
    //               buttons: [
    //                 'OK'
    //               ]
    //             }).then(res => {
    //               res.present()
    //             })
    //           }
    //           else if (pw.newP2.length < 6) { //password length less than 6
    //             this.alertCtrl.create({
    //               header: 'Alert!',
    //               message: 'New password too short!',
    //               buttons: [
    //                 'OK'
    //               ]
    //             }).then(res => {
    //               res.present()
    //             })
    //           }
    //           // else if ((pw.newP == pw.newP2) && (pw.newP.length >= 6)) { //if password is same and length is more than 6
    //           //   this.changePassword(pw.newP);
    //           // }
    //           else {
    //             this.changePassword(pw.newP);
    //           }
    //         }
    //         else {
    //           this.toastService.presentToast('Current password was entered incorrectly. Please try again.');
    //         }
    //       }
    //     }
    //   ]
    // });

    // await passwordAlert.present();
    this.toastService.presentToast("This function is disabled due to testing purposes")
  }

  async changePassword(password) {
    (await this.currentUser).updatePassword(password).then(res => {
      console.log('changed password', res);
      this.user.password = password;

      this.loginService.updatePassword(this.user.userID, password);
      this.toastService.presentToast("Change password successful!")      

    }).catch(e => {
      this.toastService.presentToast('Error! Please try again later or try to login again to change password.');
    })
  }

  async signOut() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Log Out',
      message: 'Are you sure you want to log out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.afAuth.signOut().then(() => {
              this.router.navigate(['login'])
              // window.location.reload();
              // this.navCtrl.navigateRoot(['/login']);              
            })
          }
        }
      ]
    });
    await alert.present();
    // return this.afAuth.signOut().then(() => {
    //   this.router.navigate(['login'])
    // })
  }

}
