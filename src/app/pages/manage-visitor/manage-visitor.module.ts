import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManageVisitorPageRoutingModule } from './manage-visitor-routing.module';

import { ManageVisitorPage } from './manage-visitor.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManageVisitorPageRoutingModule,
    ComponentModule
  ],
  declarations: [ManageVisitorPage]
})
export class ManageVisitorPageModule {}
