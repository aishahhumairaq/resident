import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Unit, Visitor } from 'src/app/models/unit.model';
import { VisitorService } from 'src/app/services/visitor.service';
import { shareReplay } from 'rxjs/operators';
import { ModalController } from '@ionic/angular';
import { AddVisitorComponent } from 'src/app/components/visitor/add-visitor/add-visitor.component';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-manage-visitor',
  templateUrl: './manage-visitor.page.html',
  styleUrls: ['./manage-visitor.page.scss'],
})
export class ManageVisitorPage implements OnInit {

  unit = {} as Unit;
  visitor: Visitor[];
  visPending: Visitor[] = [];
  visVisited: Visitor[] = [];
  status = 'Pending';

  constructor(
    private router: Router,
    private visitorService: VisitorService,
    private modalCtrl: ModalController,
    private menuService: MenuService
  ) { 
    if(this.router.getCurrentNavigation().extras.state) {
      this.unit = this.router.getCurrentNavigation().extras.state.data;
      console.log('visitor ts', this.unit);
    }
    // else {
    //   this.unit.unitID = "N4L5oI8ztTNDJPUcwahc"
    //   this.unit.unitNum = "01-01"
    //   this.unit.block = "A"
    // }

    if(!this.unit.unitID) {
      this.unit = this.menuService.returnUnit();
      
    }
    this.getVisitorUsingUnitID(this.unit.unitID);
  }

  ngOnInit() {
    console.log(this.unit);
  }

  segmentChanged(event){
    this.status = event.target.value
    console.log(this.status);
  }

  async getVisitorUsingUnitID(unitID: string) {
    (await this.visitorService.getVisitorUsingUnitID(unitID)).pipe(shareReplay()).subscribe( data => {
      this.visitor = data;
      console.log('visitor', this.visitor)
      this.visPending = [];
      this.visVisited = [];

      for(let i = 0; i < this.visitor.length; i++) {
        if(this.visitor[i].status == 'Pending') {
          this.visPending.push(this.visitor[i])
        }
        else if(this.visitor[i].status == 'Visited') {
          this.visVisited.push(this.visitor[i])
        }
      }

    })
  }

  async goToAddVisitor(id, block, unitNum) {
    const addVisitor = await this.modalCtrl.create({
      component: AddVisitorComponent,
      cssClass: 'add-complaint-modal',
      componentProps: {
        'id': id,
        'block': block,
        'unitNum': unitNum,
        
      }
    });
    console.log('add visitor', unitNum, block);
    
    return await addVisitor.present();
  }

}
