import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManageVisitorPage } from './manage-visitor.page';

const routes: Routes = [
  {
    path: '',
    component: ManageVisitorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageVisitorPageRoutingModule {}
