import { Component, OnInit } from '@angular/core';
import { Unit } from 'src/app/models/unit.model';
import { Profile, User } from 'src/app/models/user.model';
import { registerResidentService } from 'src/app/services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user: User = {
    unitID: '',
    profileID: '',
    userID:'',
    email: '',
    password: '',
    userType: 1, // 0: management, 1: resident
    firstLogin: true, // default: true
  }

  profile: Profile = {
    profileID: '',
    userID: '',
    fullName: '',
    icNum: '',
    birthDate: '',
    parkingID: '',
    gender: '',
    unitID: '',
  }

  unit: Unit = {
    status: '',
    userID: '',
    unitID: '',
    block: '',
    unitNum: '', 
    parkingID: '',
  }

  constructor(
    private registerService: registerResidentService
  ) { }

  ngOnInit() {
  }

  register(user: User, profile: Profile, unit: Unit) {
    console.log("user", user); 
    console.log("profile", profile);
    console.log("unit", unit);

    this.registerService.registerResident(user, profile, unit);
  }

}
