import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Invoice } from 'src/app/models/invoice.model';
import { Unit } from 'src/app/models/unit.model';
import { InvoiceService } from 'src/app/services/invoice.service';
import { shareReplay } from 'rxjs/operators';
import { MenuService } from 'src/app/services/menu.service';
import { ModalController } from '@ionic/angular';
import { InvoiceDetailsComponent } from 'src/app/components/invoice/invoice-details/invoice-details.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.page.html',
  styleUrls: ['./invoice.page.scss'],
})
export class InvoicePage implements OnInit {

  unit: Unit = {
    status: '',
    userID: '',
    unitID: '',
    block: '',
    unitNum: '', 
    parkingID: '',
  }

  invoice: Invoice[];
  invoiceBackup: any[];

  public invoiceList: any[];
  public invoiceListBackup: any[];
  
  constructor(
    private invoiceService: InvoiceService,
    private router: Router,
    private menuService: MenuService,
    private modalCtrl: ModalController,
    private firestore: AngularFirestore

  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.unit = this.router.getCurrentNavigation().extras.state.data;
      console.log('invoice get data', this.unit);
    }
    // else {
    //   this.unit.unitID = "N4L5oI8ztTNDJPUcwahc"
    //   this.unit.unitNum = "01-01"
    //   this.unit.block = "A"
    // }

    if(!this.unit.unitID) {
      this.unit = this.menuService.returnUnit()
    }
    // this.getInvoice(this.unit.unitID);
  }

  async ngOnInit() {
    this.invoice = await this.initializeItems(this.unit.unitID);
    console.log(this.invoice);
    

  }

  async initializeItems(unitID): Promise<any> {
    const invoiceList = await this.firestore.collection("invoice ", ref => ref.where("unitID", "==", unitID))
      .valueChanges().pipe(first()).toPromise();
    this.invoiceListBackup = invoiceList;
    return invoiceList;

  }

  async filterList(evt) {
    this.invoice = this.invoiceListBackup;
    console.log('get list', this.invoice);
    
    const searchTerm = evt.srcElement.value;
  
    if (!searchTerm) {
      return;
    }
  
    this.invoice = this.invoice.filter(invoice => {
      if (invoice.category && searchTerm) {
        return (invoice.category.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
      }
    });
  }

  async getInvoice(unitID: string) {

    (await this.invoiceService.getInvoiceUsingUnitID(unitID)).pipe(shareReplay()).subscribe(res => {
      this.invoice = res; 
      console.log('invoice ts', this.invoice)
    })

  }

  async openInvoiceDetails(iv) {
    const invoiceModal = await this.modalCtrl.create({
      component: InvoiceDetailsComponent,
      componentProps: {
        'invoice': iv
      }
    })

    return await invoiceModal.present();
  }

}
