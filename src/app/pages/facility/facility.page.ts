import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { Unit } from 'src/app/models/unit.model';
import { FacilityService } from 'src/app/services/facility.service';
import { MenuService } from 'src/app/services/menu.service';
import { shareReplay } from 'rxjs/operators';
import { Facility } from 'src/app/models/facility.model';


@Component({
  selector: 'app-facility',
  templateUrl: './facility.page.html',
  styleUrls: ['./facility.page.scss'],
})
export class FacilityPage implements OnInit {

  option = 'Facility';
  unit: Unit;
  facility: Facility[];
  mybooking: any;

  constructor(
    private storage: AngularFireStorage,
    private router: Router,
    private menuService: MenuService,
    private facilityService: FacilityService

  ) {

    if (this.router.getCurrentNavigation().extras.state) {
      this.unit = this.router.getCurrentNavigation().extras.state.data;
      console.log('facility', this.unit);
    }

    else {
      this.unit = this.menuService.returnUnit();
      console.log('get unit', this.unit);
      
    }
  }

  ngOnInit() {
    this.getFacility();
    this.getUnitBooking(this.unit.unitID)

  }

  segmentChanged(event) {
    this.option = event.target.value
    console.log(this.option);
  }

  async getFacility() {
    (await this.facilityService.getFacility()).pipe(shareReplay()).subscribe(res => {
      this.facility = res;
      console.log('facility', this.facility);

      for (let i = 0; i < this.facility.length; i++) {
        this.facility[i].editText = this.facility[i].facilityDesc.slice(0, 130) + '...'
      }
    })
  }

  async getUnitBooking(unitID) {
    (await this.facilityService.getUnitBooking(unitID)).pipe(shareReplay()).subscribe(res => {
      this.mybooking = res;
      console.log('facility', this.mybooking);
  })
}
}

  //nak amek gambar url facility
  // loadFiles() {

  //     const storageRef = this.storage.storage.ref('/pic-facility');
  //     storageRef.listAll().then(result => {
  //       let cloudFiles = []
  //       result.items.forEach(async ref => {
  //         cloudFiles.push({
  //           name: ref.name,
  //           full: ref.fullPath,
  //           url: await ref.getDownloadURL(),
  //           ref: ref
  //         });

  //         console.log("gambar fac", cloudFiles);
  //       });
  //     });
  // }


