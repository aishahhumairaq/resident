export interface Invoice  {
    invoiceID: string;
    unitID: string; 
    block: string; 
    unitNum: string;
    category: string; 
    amountInvoice: string;
    dueDate: any; 
}

