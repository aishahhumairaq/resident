export interface User {
    userID: string,
    email: string,
    password: string,
    userType: number, // 0: management, 1: resident
    firstLogin: boolean, // default: true
    unitID: string,
    profileID: string,
}

export interface Profile {
    profileID: string;
    userID: string;
    parkingID: string;
    fullName: string;
    icNum: string;
    birthDate: string;
    gender: string; // 0: female, 1: male
    unitID: string;
}