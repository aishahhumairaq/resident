export interface Unit {
    userID: string;
    unitID: string;
    block: string;
    unitNum: string; 
    parkingID: string;
    status: string;
}

export interface Visitor {
    unitID: string;
    visitorID: string;
    visName: string;
    visPhone: string;
    vehicle: string;
    vehiclePlate: string;
    visitDate: Date;
    status: string;
    unitNum: string;
    block: string;

}