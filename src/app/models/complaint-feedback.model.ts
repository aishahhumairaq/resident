export interface Complaint {
    complaintID?: string;
    unitID?: string; 
    complaintTitle?: string; 
    complaintDetails?: string;
    complaintDate?: any; 
    unitNum?: any;
    block?: any;
}

export interface Feedback {
    feedbackID?: string;
    unitID?: string; 
    feedbackCategory?: string;
    feedbackDetails?: string;
    suggestion?: string;
    feedbackDate?: any;
    unitNum?: any;
    block?: any;
}