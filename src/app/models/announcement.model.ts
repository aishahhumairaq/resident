export interface Announcement {
    newsID: string;
    newsTitle: any;
    newsText: any;
    newsImage: any;
    timestamp: any;
    editText: any;
}