export interface Parking {
    parkingID: string;
    parkingName: string;
    parkingNum: string;
    unitID: string;
    vehicleMod: string;
    vehiclePlate: string;
    status: any;
    unitNum: string;
    block: string;


}