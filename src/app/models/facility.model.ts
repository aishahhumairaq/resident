export interface Facility {
    facilityID: string;
    facilityName: string;
    facilityDesc: string;
    facilityImage: any;
    status: number; //0-available, 1-unavailable
    editText: string;
}

export interface Booking {
    block: string;
    unitNum: string;
    unitID: string;
    date: string;
    slot: string;
    facilityName: string;
    facilityImage: any;
    facilityID: string;
    id: any;
}

// export interface UnavailableSlots {
//     block: string;
//     facilityName: string;
//     facilityID: string;
//     id: string; //doc id yg letak unavailable time slots
//     date: string;
//     unitID: string;
//     unitNum: string;
    
// }