import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Invoice } from 'src/app/models/invoice.model';

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.scss'],
})
export class InvoiceDetailsComponent implements OnInit {

  @Input() invoice: Invoice;

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {}

  backToPage() {
    this.modalCtrl.dismiss()
  }

  close() {
    this.modalCtrl.dismiss()
  }

}
