import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';
import { Announcement } from 'src/app/models/announcement.model';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss'],
})
export class EditPostComponent implements OnInit {

  @Input() post: Announcement;
  formerText: any;

  constructor(
    private modalCtrl: ModalController,
    private firestore: AngularFirestore
  ) { }

  ngOnInit() {
    console.log('post id', this.post.newsID);
    this.formerText = this.post.newsText;
  }

  async updatePost(post: Announcement) {

    // this.firestore.doc("announcement/"+ post.newsID).valueChanges().subscribe(data => {
    //   this.post.newsTitle = data["newsTitle"];
    //   this.post.newsText = data["newsText"];
    //   this.post.newsImage = data["newsImage"];
    //   this.post.timestamp = data["timestamp"];
    // })
    await this.firestore.doc("announcement/" + post.newsID).update(post);
    this.modalCtrl.dismiss();
    console.log('post details', post)
  }
  
  
  cancel() {
    this.modalCtrl.dismiss();
    this.post.newsText = this.formerText;
    console.log('post cancel', this.post.newsText)
  }
}