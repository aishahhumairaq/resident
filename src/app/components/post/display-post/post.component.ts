import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
import { Announcement } from 'src/app/models/announcement.model';
import { ToastService } from 'src/app/services/toast.service';
import { EditPostComponent } from '../edit-post/edit-post.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {

  @Input() post: Announcement;
  id: any;

  constructor(
    private modalCtrl: ModalController,
    private firestore: AngularFirestore,
    private navCtrl: NavController,
  ) { 

  }

  ngOnInit() {
    console.log('specific post details', this.post.newsID)

  }


  back() {
    this.modalCtrl.dismiss();
  }

  close() {
    this.modalCtrl.dismiss();
  }
  
  // async editPostPage(post) {

  //   const editPostModal = await this.modalCtrl.create({
  //     component: EditPostComponent,
  //     cssClass: 'editpost-modal',
  //     componentProps: {
  //       'post': post

  //     }
  //   });
  //   console.log('post deets', post);
  //   return await editPostModal.present();

  // }


  // async deletePost(newsID) {

  //   await this.firestore.doc("announcement/" + newsID).delete();
  //   this.modalCtrl.dismiss();
  // }

}
