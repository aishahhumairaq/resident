import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Feedback } from 'src/app/models/complaint-feedback.model';

@Component({
  selector: 'app-feedback-details',
  templateUrl: './feedback-details.component.html',
  styleUrls: ['./feedback-details.component.scss'],
})
export class FeedbackDetailsComponent implements OnInit {

  @Input() feedback: Feedback;
  @Input() block: any;
  @Input() unitNum: any;
   
  constructor(
    private modalCtrl: ModalController,
    private location: Location
  ) { }

  ngOnInit() {}

  backToPage(){
    this.modalCtrl.dismiss();
  }

  close() {
    this.modalCtrl.dismiss();
  }

  convertDate() {
  }
}
