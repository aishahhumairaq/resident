import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Complaint, Feedback } from 'src/app/models/complaint-feedback.model';
import { ComplaintDetailsComponent } from '../complaint-details/complaint-details.component';
import { FeedbackDetailsComponent } from '../feedback-details/feedback-details.component';

@Component({
  selector: 'app-display-all',
  templateUrl: './display-all.component.html',
  styleUrls: ['./display-all.component.scss'],
})
export class DisplayAllComponent implements OnInit {

  @Input() feedbacks: Complaint;
  @Input() complaints: Feedback;
  @Input() id: any;
  @Input() block: any;
  @Input() unitNum: any;

  constructor(
    private modal: ModalController
  ) { }

  ngOnInit() {

  }

  async goToComplaintModal(complaint) {
    const complaintModal = await this.modal.create({
      component: ComplaintDetailsComponent,
      cssClass: 'complaint-modal',
      componentProps: {
        'complaint': complaint,
        'block': this.block,
        'unitNum': this.unitNum

      }
    });
    console.log('complaint deets', complaint)
    return await complaintModal.present();

  }

  async goToFeedbackModal(feedback) {
    const feedbackModal = await this.modal.create({
      component: FeedbackDetailsComponent,
      cssClass: 'feedback-modal',
      componentProps: {
        'feedback': feedback,
        'block': this.block,
        'unitNum': this.unitNum
      }
    });
    console.log('feedback deets', feedback)
    return await feedbackModal.present()

    // let res = await feedbackModal.onDidDismiss();
    // console.log('dismiss modal: ', res)
  }

}
