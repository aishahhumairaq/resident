import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Feedback } from 'src/app/models/complaint-feedback.model';
import { AddFeedbackComponent } from '../add-feedback/add-feedback.component';
import { FeedbackDetailsComponent } from '../feedback-details/feedback-details.component';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit {

  @Input() feedbacks: Feedback;
  @Input() id: any;
  @Input() block: any;
  @Input() unitNum: any;
  
  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    console.log('inputs',this.id, this.block, this.unitNum);
    
  }

  async goToFeedbackModal(feedback) {
    const feedbackModal  = await this.modalCtrl.create({
      component: FeedbackDetailsComponent,
      cssClass: 'feedback-modal',
      componentProps: {
        'feedback': feedback,
        'unitNum': this.unitNum,
        'block': this.block

      }
    });
    console.log('feedback deets', feedback)
    return await feedbackModal.present();
  }

  async goToAddFeedback(id, unitNum, block) {
    console.log('add feedback', id,unitNum, block);
    
    const addFeedback = await this.modalCtrl.create({
      component: AddFeedbackComponent,
      cssClass: 'add-feedback-modal',
      componentProps: {
        'unitNum': unitNum,
        'block': block,
        'id': id

      }
      
    });
    return await addFeedback.present();
    
  }
}
