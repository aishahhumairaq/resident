import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { Complaint } from 'src/app/models/complaint-feedback.model';
import { ComplaintService } from 'src/app/services/complaint.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-add-complaint',
  templateUrl: './add-complaint.component.html',
  styleUrls: ['./add-complaint.component.scss'],
})
export class AddComplaintComponent implements OnInit {

  @Input() block: any;
  @Input() unitNum: any;
  @Input() id: any;

  complaint: Complaint = {
    unitNum: '',
    block: '',
    unitID: '',
    complaintTitle: '',
    complaintDetails: '',
    complaintDate: new Date
  }

  constructor(
    private location: Location,
    private complaintService: ComplaintService,
    private modal: ModalController,
    private toastService: ToastService

  ) { }

  ngOnInit() {
    console.log('block', this.block)
    console.log('unit', this.unitNum)
    console.log('id', this.id)

  }

  cancel() {
    this.modal.dismiss();
  }

  async submitComplaint(complaint: Complaint) {
    console.log(complaint);
    this.complaintService.complaintValidation(complaint, this.id, this.block, this.unitNum).then(res => {
      
    })
  }

}
