import { createComponentDefinitionMap } from '@angular/compiler/src/render3/partial/component';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Complaint } from 'src/app/models/complaint-feedback.model';
import { AddComplaintComponent } from '../add-complaint/add-complaint.component';
import { ComplaintDetailsComponent } from '../complaint-details/complaint-details.component';

@Component({
  selector: 'app-complaint',
  templateUrl: './complaint.component.html',
  styleUrls: ['./complaint.component.scss'],
})
export class ComplaintComponent implements OnInit {

  @Input() id: any;
  @Input() complaints: Complaint;
  @Input() unitNum: any;
  @Input() block: any;

  constructor(
    private modal: ModalController,
  ) { 
  }

  ngOnInit() {
    console.log('complaint', this.complaints)
    console.log('id', this.id)
  }

  ionViewWillEnter() {
    
  }

  async goToComplaintModal(complaint) {
    const complaintModal = await this.modal.create({
      component: ComplaintDetailsComponent,
      cssClass: 'complaint-modal',
      componentProps: {
        'complaint': complaint,
        'unitNum': this.unitNum,
        'block': this.block

      }
    });
    console.log('complaint deets', complaint)
    return await complaintModal.present();

  }

  async goToAddComplaint(id, unitNum, block) {
    const addComplaint = await this.modal.create({
      component: AddComplaintComponent,
      cssClass: 'add-complaint-modal',
      componentProps: {
        'unitNum': unitNum,
        'block': block,
        'id': id

      }
      
    });
    return await addComplaint.present();

    // console.log('created')
    // let res = await addComplaint.onDidDismiss();
    // console.log('dismiss modal: ', res)
  }


}
