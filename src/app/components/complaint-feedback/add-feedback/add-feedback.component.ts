import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Feedback } from 'src/app/models/complaint-feedback.model';
import { FeedbackService } from 'src/app/services/feedback.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-add-feedback',
  templateUrl: './add-feedback.component.html',
  styleUrls: ['./add-feedback.component.scss'],
})
export class AddFeedbackComponent implements OnInit {

  @Input() block: any;
  @Input() unitNum: any;
  @Input() id: any;

  feedback: Feedback = {
    block: '',
    unitNum: '',
    unitID: '',
    feedbackID: '',
    feedbackCategory: '',
    feedbackDetails: '',
    suggestion: '',
    feedbackDate: new Date
  }


  constructor(
    private feedbackService: FeedbackService,
    private modal: ModalController,
    private toastService: ToastService
  ) { 

  }

  ngOnInit() {}

  cancel() {
    this.modal.dismiss()
  }

  submitFeedback(feedback) {
    console.log(feedback)
    this.feedbackService.feedbackValidation(feedback, this.id, this.unitNum, this.block).then(res => {
    
    })
  
  }

}
