import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Complaint } from 'src/app/models/complaint-feedback.model';

@Component({
  selector: 'app-complaint-details',
  templateUrl: './complaint-details.component.html',
  styleUrls: ['./complaint-details.component.scss'],
})
export class ComplaintDetailsComponent implements OnInit {

  @Input() complaint: Complaint[];
  @Input() block: any; 
  @Input() unitNum: any;

  constructor(
    private modalCtrl: ModalController,
    private location: Location
  ) { 
    
  }

  ngOnInit() {

  }

  backToPage(){
    this.modalCtrl.dismiss();
  }

  close() {
    this.modalCtrl.dismiss();
  }

  convertDate() {
  }

}
