import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';
import { Profile, User } from 'src/app/models/user.model';
import { MenuService } from 'src/app/services/menu.service';
import { ProfileService } from 'src/app/services/profile.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-first-profile',
  templateUrl: './first-profile.component.html',
  styleUrls: ['./first-profile.component.scss'],
})
export class FirstProfileComponent implements OnInit {

  @Input() userID: any;
  profile = {} as Profile;

  constructor(
    private profileService: ProfileService,
    private modal: ModalController,
    private firestore: AngularFirestore,
    private toastService: ToastService
  ) {
    
  }

  ngOnInit() {
    this.getProfile()
    console.log('userid', this.userID);
    
  }

  getProfile() {
    this.profileService.getProfile(this.userID).then(profile => {
      this.profile = profile; 
      console.log('get profile',);
      
    })
  }

  async updateMyProfile(profile) {
    if(profile.fullName != '' && profile.icNum != '' && profile.birthDate != '' && profile.gender != '') {
      await this.firestore.doc('profiles/' + profile.profileID).update(profile);
      console.log('update', profile)
      this.toastService.presentToast('You have successfully updated your profile!')
      this.modal.dismiss();
    }
    else {
      this.toastService.presentToast('Please fill in every text fields before proceed')
    }

  }

}
