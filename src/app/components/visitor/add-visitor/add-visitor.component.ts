import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Unit, Visitor } from 'src/app/models/unit.model';
import { ToastService } from 'src/app/services/toast.service';
import { VisitorService } from 'src/app/services/visitor.service';

@Component({
  selector: 'app-add-visitor',
  templateUrl: './add-visitor.component.html',
  styleUrls: ['./add-visitor.component.scss'],
})
export class AddVisitorComponent implements OnInit {

  @Input() id: any;
  @Input() block: any;
  @Input() unitNum: any;

  visitor = {} as Visitor;

  unit = {} as Unit;

  visitDate: any = new Date().toISOString();

  constructor(
    private modalCtrl: ModalController,
    private visitorService: VisitorService,
    private modal: ModalController,
    private toastService: ToastService

  ) {
    // this.visitor.visitDate = new Date();
  }

  ngOnInit() {
    console.log(this.id, this.block, this.unitNum)
  }

  cancel() {
    this.modalCtrl.dismiss();
  }


  async addVisitor(visitor: Visitor) {
    console.log('add vis', visitor, this.id);

    this.visitorService.registerVisitor(visitor, this.id, this.block, this.unitNum).then(res => {
     
    })
  }
  
  ionViewWillLeave() {
    this.visitor.visitDate = null;
  }

}
