import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController, ModalController } from '@ionic/angular';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-visitor-details',
  templateUrl: './visitor-details.component.html',
  styleUrls: ['./visitor-details.component.scss'],
})
export class VisitorDetailsComponent implements OnInit {

  @Input() details: any;

  constructor(
    private modalCtrl: ModalController,
    private firestore: AngularFirestore,
    private toastService: ToastService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    console.log('pending pop', this.details);

  }

  close() {
    this.modalCtrl.dismiss();
  }

  async delete(visitorID) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm',
      message: 'Are you sure you want to delete this visitor details?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Okay');
            this.firestore.doc("visitor/" + visitorID).delete();
            this.modalCtrl.dismiss();
            this.toastService.presentToast("Successfully deleted a visitor details!")
          }
        }
      ]
    });
    await alert.present();
  }

  async updateDetails(details) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm',
      message: 'Are you sure you want to update your visitor details?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.firestore.doc('visitor/' + details.visitorID).update(details);
            console.log('update', details)
            this.modalCtrl.dismiss();
            this.toastService.presentToast('Successfully updated your visitor status!')
          }
        }
      ]
    });
    await alert.present();

  }

}
