import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { VisitorDetailsComponent } from '../visitor-details/visitor-details.component';

@Component({
  selector: 'app-visited',
  templateUrl: './visited.component.html',
  styleUrls: ['./visited.component.scss'],
})
export class VisitedComponent implements OnInit {

  @Input() id: any;
  @Input() block: any;
  @Input() unitNum: any;
  @Input() visited: any;
  
  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {}

  async goToVisitorDetails(visitor) {
    const visitorModal = await this.modalCtrl.create({
      component: VisitorDetailsComponent,
      cssClass: 'visitor-details',
      componentProps: {
        'details': visitor,
      }

    });
    return await visitorModal.present();
  }
}
