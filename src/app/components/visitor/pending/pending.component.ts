import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { VisitorDetailsComponent } from '../visitor-details/visitor-details.component';

@Component({
  selector: 'app-pending',
  templateUrl: './pending.component.html',
  styleUrls: ['./pending.component.scss'],
})
export class PendingComponent implements OnInit {

  @Input() id: any;
  @Input() block: any;
  @Input() unitNum: any;
  @Input() pending: any;

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    
  }

  async goToVisitorDetails(visitor) {
    const visitorModal = await this.modalCtrl.create({
      component: VisitorDetailsComponent,
      cssClass: 'visitor-details',
      componentProps: {
        'details': visitor,
      }

    });
    await visitorModal.present();

  }

}
