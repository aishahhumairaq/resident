import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {

  @Input() options: any;

  date: any;
  constructor(
    private modal: ModalController
  ) { }

  ngOnInit() {}

  cancel() {
    this.modal.dismiss();
  }

  done() {
    this.modal.dismiss({
      date: this.date
    });
  }
}
