import { Component, Input, OnInit } from '@angular/core';
import { Facility } from 'src/app/models/facility.model';
import { Unit } from 'src/app/models/unit.model';
import { FacilityService } from 'src/app/services/facility.service';
import { MenuService } from 'src/app/services/menu.service';
import { shareReplay } from 'rxjs/operators';
import { ModalController } from '@ionic/angular';
import { BookingDetailsComponent } from '../booking-details/booking-details.component';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss'],
})
export class BookingsComponent implements OnInit {

  @Input() facility: Facility;
  @Input() unit: Unit;
  @Input() mybooking: any;

  constructor(
    private menuService: MenuService,
    private facilityService: FacilityService,
    private modalCtrl: ModalController
  ) {
    this.unit = this.menuService.returnUnit();
   }

  ngOnInit() {
    console.log('unit', this.unit);
    console.log('mybookings', this.mybooking);
  
  }

  async openBookingDetails(book) {
    const bookDetails  = await this.modalCtrl.create({
      component: BookingDetailsComponent,
      componentProps: {
        'book': book
      }
    });
    console.log('book details', book)
    await bookDetails.present();
  }

}
