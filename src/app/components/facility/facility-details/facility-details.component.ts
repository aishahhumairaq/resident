import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Facility } from 'src/app/models/facility.model';
import { Unit } from 'src/app/models/unit.model';
import { FacilityService } from 'src/app/services/facility.service';
import { BookFacilityComponent } from '../book-facility/book-facility.component';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-facility-details',
  templateUrl: './facility-details.component.html',
  styleUrls: ['./facility-details.component.scss'],
})
export class FacilityDetailsComponent implements OnInit {

  @Input() facility: Facility;
  @Input() unit: Unit;
  ophours: any;

  constructor(
    private modalCtrl: ModalController,
    private facilityService: FacilityService
  ) { }

  ngOnInit() {
    console.log('unit', this.unit);
    console.log('this facility info', this.facility);
    this.getOperationHours(this.facility.facilityID);
  }

  back() {
    this.modalCtrl.dismiss();
  }

  async getOperationHours(facilityID) {
    (await this.facilityService.getSlots(facilityID)).pipe(shareReplay()).subscribe(data => {
      this.ophours = data;
      console.log('ophours', this.ophours);
      this.sortMyDays();
    })
  }

  sortMyDays() {
    let sortedArray = [];
    this.ophours.forEach(day => {
      if (day.id == 'MONDAY') {
        sortedArray[0] = day;
      }
      else if (day.id == 'TUESDAY') {
        sortedArray[1] = day;
      }
      else if (day.id == 'WEDNESDAY') {
        sortedArray[2] = day;
      }
      else if (day.id == 'THURSDAY') {
        sortedArray[3] = day;
      }
      else if (day.id == 'FRIDAY') {
        sortedArray[4] = day;
      }
      else if (day.id == 'SATURDAY') {
        sortedArray[5] = day;
      }
      else if (day.id == 'SUNDAY') {
        sortedArray[6] = day;
      }
    })
    console.log('sortedArray', sortedArray);
    this.ophours = sortedArray;
  }

  async bookFacility() {
    const bookFacility = await this.modalCtrl.create({
      component: BookFacilityComponent,
      componentProps: {
        'facility': this.facility,
        'unit': this.unit
      }
    })

    return await bookFacility.present();
  }

}
