import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Facility } from 'src/app/models/facility.model';
import { Unit } from 'src/app/models/unit.model';
import { FacilityDetailsComponent } from '../facility-details/facility-details.component';

@Component({
  selector: 'app-all-facilities',
  templateUrl: './all-facilities.component.html',
  styleUrls: ['./all-facilities.component.scss'],
})
export class AllFacilitiesComponent implements OnInit {

  @Input() facility: Facility;
  @Input() unit: Unit;

  constructor(
    private modal: ModalController
  ) { }

  ngOnInit() {
    console.log('unit', this.unit);
    
  }

  async goToFacility(facility) {
    const facilityModal = await this.modal.create({
      component: FacilityDetailsComponent,
      componentProps: {
        'facility': facility,
        'unit': this.unit
      }
    });

    return await facilityModal.present()
  }
}
