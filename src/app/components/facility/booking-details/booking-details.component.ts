import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController, ModalController } from '@ionic/angular';
import { FacilityService } from 'src/app/services/facility.service';
import { shareReplay } from 'rxjs/operators';
import { ToastService } from 'src/app/services/toast.service';
import { Booking } from 'src/app/models/facility.model';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.scss'],
})
export class BookingDetailsComponent implements OnInit {

  @Input() book: Booking;
  
  constructor(
    private modalCtrl: ModalController,
    private firestore: AngularFirestore,
    private facilityService: FacilityService,
    private toastService: ToastService,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    console.log('mybook', this.book);
  }

  // //from booking table in firebase
  // async getUnavailableSlots(facilityID, date, unitID) {
  //   (await this.facilityService.getBookedSlots(facilityID, date, unitID)).pipe(shareReplay()).subscribe(data => {
  //     this.unavailableslot = data;
  //     console.log('booked slot', this.unavailableslot);
  //   })
  // }

  backToPage() {
    this.modalCtrl.dismiss()
  }

  async cancelBooking() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Confirm',
      message: 'Are you sure you want to cancel your booking?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, 
        {
          text: 'Yes',
          handler: () => {
            this.firestore.collection("booking").doc(this.book.facilityID).collection(this.book.date).doc(this.book.slot).delete();
            this.firestore.doc("user-booking/" + this.book.id).delete();
            this.modalCtrl.dismiss();
            this.toastService.presentToast("You successfully canceled a booking!");
          }
        }
      ]
    });
    await alert.present();
   
  }

}
