import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Facility } from 'src/app/models/facility.model';
import { Unit } from 'src/app/models/unit.model';
import { FacilityService } from 'src/app/services/facility.service';
import { shareReplay } from 'rxjs/operators';
import { AlertController, ModalController } from '@ionic/angular';
import { CalendarComponentOptions, CalendarModal, CalendarModalOptions, CalendarResult } from 'ion2-calendar';
import * as moment from 'moment';
import { CalendarComponent } from '../calendar/calendar.component';
import { ToastService } from 'src/app/services/toast.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-book-facility',
  templateUrl: './book-facility.component.html',
  styleUrls: ['./book-facility.component.scss'],
})
export class BookFacilityComponent implements OnInit {

  @Input() facility: Facility;
  @Input() unit: Unit;
  slots: any;
  date: any;

  book: any;
  availableSlots: any;
  day: any;
  timeslot: any;

  bookedSlot: any;

  constructor(
    private firestore: AngularFirestore,
    private facilityService: FacilityService,
    private modalCtrl: ModalController,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private alertCtrl: AlertController
  ) {

  }

  ngOnInit() {
    console.log('unit', this.unit);
    console.log('facility', this.facility);
    this.date = 'Select a date'
    this.getAvailableSlots(this.facility.facilityID)
  }

  async getAvailableSlots(facilityID) {
    (await this.facilityService.getSlots(facilityID)).pipe(shareReplay()).subscribe(data => {
      this.slots = data;
      console.log('slots', this.slots);
    })
  }

  //get booked slots from booking table in firebase 
  async getBooking(facilityID, date) {
    // this.loadingService.show();
    (await this.facilityService.getBooking(facilityID, date)).pipe(shareReplay()).subscribe(booking => {
      this.bookedSlot = booking;
      console.log('booked slot***********', this.bookedSlot);
      // this.loadingService.hide();
      this.pickSlot();


    })
  }

  async openCalendar() {
    let disabled = [0, 1, 2, 3, 4, 5, 6];
    let enabled = [];
    this.slots.forEach(slot => {
      if (slot.id == 'MONDAY') {
        enabled.push(1)
      }
      else if (slot.id == 'TUESDAY') {
        enabled.push(2)
      }
      else if (slot.id == 'WEDNESDAY') {
        enabled.push(3)
      }
      else if (slot.id == 'THURSDAY') {
        enabled.push(4)
      }
      else if (slot.id == 'FRIDAY') {
        enabled.push(5)
      }
      else if (slot.id == 'SATURDAY') {
        enabled.push(6)
      }
      else if (slot.id == 'SUNDAY') {
        enabled.push(0)
      }
    });

    enabled.forEach(enable => {
      let id = disabled.indexOf(enable);
      disabled.splice(id, 1);
    })

    console.log('disabled', disabled);
    console.log('enabled', enabled);


    const options: CalendarComponentOptions = {
      to: new Date(moment().add(14, 'days').toDate()), //can book only 7 days in advance
      disableWeeks: disabled

    };
    const myCalendar = await this.modalCtrl.create({
      component: CalendarComponent,
      componentProps: {
        options
      }
    });

    await myCalendar.present();
    const event: any = await myCalendar.onDidDismiss();
    this.book = moment(event.data.date).format('dddd, DD MMMM YYYY');
    this.day = this.book.split(',');
    console.log(this.book);
    this.date = this.book;
    console.log('get day', this.day);

    this.getBooking(this.facility.facilityID, this.book)

  }

  async pickSlot() {
    this.availableSlots = [];
    console.log('date booked', this.book);
    this.slots.forEach(slot => {
      if (this.day[0].toUpperCase() == slot.id) {
        this.availableSlots.push(slot.slot);
        console.log('avail slot', this.availableSlots);
      }
    })
    this.removeSlot();
    // for (let i = 0; i < this.slots.length; i++) {
    //   if (this.day[0].toUpperCase() == this.slots[i].id) {
    //     this.availableSlots.push(this.slots[i].slot);
    //     console.log('this slot', this.availableSlots);
    //   }
    //   if ((i+1) == this.slots.length){
    //     this.removeSlot();
    //     break;
    //   }
    // }
  }

  async removeSlot() {
    console.log('bookedSlot remove slot', this.bookedSlot);

    this.bookedSlot.forEach(booked => {
      console.log('booked', booked.id);
      console.log('availba]', this.availableSlots[0]);

      let id = this.availableSlots[0].indexOf(booked.id);
      if (id != -1) {
        this.availableSlots[0].splice(id, 1);
      }
      console.log('id index', id);

    })
  }

  async bookFacility() {
    if (this.validation()) {
      await this.firestore.collection("booking").doc(this.facility.facilityID).collection(this.book).
        doc(this.timeslot).set({
          block: this.unit.block,
          unitID: this.unit.unitID,
          unitNum: this.unit.unitNum,
          id: this.timeslot,
          facilityName: this.facility.facilityName,
          facilityID: this.facility.facilityID,
          date: this.book
        })
      this.modalCtrl.dismiss();
      this.toastService.presentToast("You have successfully booked a facility!");
      // let autoID = this.firestore.createId();
      await this.firestore.collection("user-booking")
        .add({
          date: this.book,
          facilityID: this.facility.facilityID,
          facilityName: this.facility.facilityName,
          slot: this.timeslot,
          unitID: this.unit.unitID,
          block: this.unit.block,
          unitNum: this.unit.unitNum,
          facilityImage: this.facility.facilityImage
        })

    }
    
    // if (this.book != '' && this.timeslot != '') {
    //   try {
    //     await this.firestore.collection("booking").doc(this.facility.facilityID).collection(this.book).
    //       doc(this.timeslot).set({
    //         block: this.unit.block,
    //         unitID: this.unit.unitID,
    //         unitNum: this.unit.unitNum,
    //         id: this.timeslot,
    //         facilityName: this.facility.facilityName,
    //         facilityID: this.facility.facilityID,
    //         date: this.book
    //       })
    //     this.modalCtrl.dismiss();
    //     this.toastService.presentToast("You have successfully booked a facility!");
    //     // let autoID = this.firestore.createId();
    //     await this.firestore.collection("user-booking")
    //       .add({
    //         date: this.book,
    //         facilityID: this.facility.facilityID,
    //         facilityName: this.facility.facilityName,
    //         slot: this.timeslot,
    //         unitID: this.unit.unitID,
    //         block: this.unit.block,
    //         unitNum: this.unit.unitNum,
    //         facilityImage: this.facility.facilityImage
    //       })

    //   }

    //   catch (e) {
    //     console.log('err', e);

    //   }
    // }
    // else {
    //   this.modalCtrl.dismiss();
    //   this.toastService.presentToast("Book facility failed as day and time was not selected!")
    // }
  }

  validation() {
    if (!this.book) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Please select a date!',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if (!this.timeslot) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Please select a timeslot!',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      return false;
    }
    else if(this.book == null && this.timeslot == null) {
      this.alertCtrl.create({
        header: 'Alert!',
        message: 'Please choose date and timeslot!',
        buttons: [
          'OK'
        ]
      }).then(res => {
        res.present()
      })
      this.modalCtrl.dismiss();
      this.toastService.presentToast("You failed to book a facility!")
      return false;

    }
    else {
      return true;
    }

  }

  cancel() {
    this.modalCtrl.dismiss()
  }


}
