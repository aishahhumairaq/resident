import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post/display-post/post.component';
import { EditPostComponent } from './post/edit-post/edit-post.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ComplaintComponent } from './complaint-feedback/complaint/complaint.component';
import { FeedbackComponent } from './complaint-feedback/feedback/feedback.component';
import { DisplayAllComponent } from './complaint-feedback/display-all/display-all.component';
import { AddComplaintComponent } from './complaint-feedback/add-complaint/add-complaint.component';
import { FeedbackDetailsComponent } from './complaint-feedback/feedback-details/feedback-details.component';
import { ComplaintDetailsComponent } from './complaint-feedback/complaint-details/complaint-details.component';
import { AddFeedbackComponent } from './complaint-feedback/add-feedback/add-feedback.component';
import { PendingComponent } from './visitor/pending/pending.component';
import { VisitedComponent } from './visitor/visited/visited.component';
import { VisitorDetailsComponent } from './visitor/visitor-details/visitor-details.component';
import { AddVisitorComponent } from './visitor/add-visitor/add-visitor.component';
import { FirstProfileComponent } from './first-profile/first-profile.component';
import { InvoiceDetailsComponent } from './invoice/invoice-details/invoice-details.component';
import { FacilityDetailsComponent } from './facility/facility-details/facility-details.component';
import { AllFacilitiesComponent } from './facility/all-facilities/all-facilities.component';
import { BookingsComponent } from './facility/bookings/bookings.component';
import { BookFacilityComponent } from './facility/book-facility/book-facility.component';
import { CalendarComponent } from './facility/calendar/calendar.component';
import { CalendarModule } from 'ion2-calendar';
import { BookingDetailsComponent } from './facility/booking-details/booking-details.component';


@NgModule({
  declarations: [PostComponent, EditPostComponent, ComplaintComponent, FeedbackComponent,
    DisplayAllComponent, AddComplaintComponent, AddFeedbackComponent, AddVisitorComponent,
    FeedbackDetailsComponent, ComplaintDetailsComponent, PendingComponent, VisitedComponent, 
    VisitorDetailsComponent, FirstProfileComponent, InvoiceDetailsComponent, FacilityDetailsComponent,
    AllFacilitiesComponent, BookingsComponent, BookFacilityComponent, CalendarComponent,
    BookingDetailsComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    CalendarModule

  ],
  exports: [
    PostComponent,
    EditPostComponent,
    ComplaintComponent,
    FeedbackComponent,
    DisplayAllComponent,
    AddComplaintComponent,
    AddFeedbackComponent,
    FeedbackDetailsComponent,
    ComplaintDetailsComponent,
    PendingComponent,
    VisitedComponent,
    VisitorDetailsComponent,
    AddVisitorComponent,
    FirstProfileComponent,
    InvoiceDetailsComponent,
    FacilityDetailsComponent,
    AllFacilitiesComponent,
    BookingsComponent,
    BookFacilityComponent,
    CalendarComponent,
    BookingDetailsComponent
  ]
})
export class ComponentModule { }
